/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mintic.easyparking.controller;

import com.mintic.easyparking.model.Bahia;
import com.mintic.easyparking.servicio.BahiaServicio;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author LEONARDO
 */

@Controller
@Slf4j
public class BahiaController {
    
    @Autowired
    private BahiaServicio bahiaServicio;
    
    @GetMapping("/bahia")
    public String bahia(Model model){
        List<Bahia> bahias = bahiaServicio.listarBahias();
        model.addAttribute("Bahias", bahias );
        return "bahia/bahia";
    }
    
}
