/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mintic.easyparking.controller;

import com.mintic.easyparking.model.TipoBahia;
import com.mintic.easyparking.servicio.TipoBahiaServicio;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j
public class TipoBahiaController {
      
    @Autowired
    private TipoBahiaServicio tipoBahiaServicio;
   
    
    @GetMapping("/tiposBahias")
    public String tiposbahia(Model model){
        List<TipoBahia> tiposBahias = tipoBahiaServicio.finAll();
        model.addAttribute("tiposBahias", tiposBahias);
        
        return "tiposBahia/tiposBahias";
    }
    
    @GetMapping("/agregarTipoBahia")
    public String agregar(TipoBahia tipoBahia){
        return "tiposBahia/tipoBahiaModificar";
    }
    
    @PostMapping("/guardarTipoBahia") // Este nombre es el mismo que se coloca en el HTML
    public String guardar(TipoBahia tipoBahia){
        tipoBahiaServicio.save(tipoBahia);
        return "redirect:/tiposBahias";
    }
    
    @GetMapping("/editar/{idtipos}")
    public String editar(TipoBahia tipoBahia, Model model){
        tipoBahia = tipoBahiaServicio.encontrarTipoBahia(tipoBahia);
        model.addAttribute("tipoBahia", tipoBahia);
        return "tiposBahia/tipoBahiaModificar";
    }
    
    @GetMapping ("/eliminar")
    public String eliminar(TipoBahia tipoBahia){
        tipoBahiaServicio.eliminar(tipoBahia);
        return "redirect:/tiposBahias";
    }
}
