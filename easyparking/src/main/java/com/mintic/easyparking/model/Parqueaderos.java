/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mintic.easyparking.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author LEONARDO
 */

@Entity
@Table(name = "parqueadero")
public class Parqueaderos implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name="cod_parq")
    private Integer cod_parq;
    @Column(name="nit")
    private String nit;
    @Column(name="razon_social")
    private String razon_social;
    @Column(name="direccion")
    private String direccion;
    @Column(name="telefono")
    private String telefono;

    public Parqueaderos() {
    }

    public Parqueaderos(Integer id, Integer cod_parq, String nit, String razon_social, String direccion, String telefono) {
        this.id = id;
        this.cod_parq = cod_parq;
        this.nit = nit;
        this.razon_social = razon_social;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCod_parq() {
        return cod_parq;
    }

    public void setCod_parq(Integer cod_parq) {
        this.cod_parq = cod_parq;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


    
}
