/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mintic.easyparking.model;

import com.sun.istack.NotNull;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author LEONARDO
 */

@Entity
@Table(name="tipobahia")
public class TipoBahia implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idtipos;
    @Column (name="tipos")
    private String tipos;
    @Column (name="codigo")
    private String codigo;

    public TipoBahia() {
    }

    public TipoBahia(Integer idtipos, String tipos, String codigo) {
        this.idtipos = idtipos;
        this.tipos = tipos;
        this.codigo = codigo;
    }

     public Integer getIdtipos() {
        return idtipos;
    }

    public void setIdtipos(Integer idtipos) {
        this.idtipos = idtipos;
    }
    
  

    public String getTipos() {
        return tipos;
    }

    public void setTipos(String tipos) {
        this.tipos = tipos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    
    
    
    
}
