/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mintic.easyparking.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

/**
 *
 * @author LEONARDO
 */
@Entity
@Table(name = "bahias")
public class Bahia implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="tipo_bahia")
    private Integer tipo_bahia;
    @Column(name="cod_bahia")
    private Integer cod_bahia;
    @Column(name="estado")
    private String estado;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipobahia_Id")
    @JsonBackReference
	private TipoBahia tipos;

    
    @Column(name="created_at", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP",nullable = false, insertable = false, updatable = false)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date createdAt;
    
    @Column(name="updated_at", columnDefinition="TIMESTAMP DEFAULT NULL",nullable = true)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateAt;

    @PreUpdate
    protected void onUpdate(){
        this.updateAt = new Date();
    } 
    
    public Bahia() {
    }

    public Bahia(Integer tipo_bahia, Integer cod_bahia, String estado) {
        this.tipo_bahia = tipo_bahia;
        this.cod_bahia = cod_bahia;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoBahia getTipos() {
        return tipos;
    }

    public void setTipos(TipoBahia tipos) {
        this.tipos = tipos;
    }

    
    public Integer getTipo_bahia() {
        return tipo_bahia;
    }

    public void setTipo_bahia(Integer tipo_bahia) {
        this.tipo_bahia = tipo_bahia;
    }

    public Integer getCod_bahia() {
        return cod_bahia;
    }

    public void setCod_bahia(Integer cod_bahia) {
        this.cod_bahia = cod_bahia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    
    
}
