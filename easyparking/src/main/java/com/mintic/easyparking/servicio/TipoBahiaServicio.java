/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.easyparking.servicio;

import com.mintic.easyparking.model.TipoBahia;
import java.util.List;

/**
 *
 * @author LEONARDO
 */
public interface TipoBahiaServicio {

    public List<TipoBahia> listarTipoBahias();

    public void guardar(TipoBahia tipoBahia);

    public void eliminar(TipoBahia tipoBahia);

    public List<TipoBahia> finAll();

    public TipoBahia encontrarTipoBahia(TipoBahia tipoBahia);

    // RestController
    
    public TipoBahia save(TipoBahia tipoBahia);

    public TipoBahia findById(Integer id);

    public void delete(Integer id);
}
