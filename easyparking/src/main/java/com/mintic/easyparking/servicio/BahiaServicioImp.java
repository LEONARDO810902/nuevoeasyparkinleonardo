/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mintic.easyparking.servicio;

import com.mintic.easyparking.dao.BahiaDao;
import com.mintic.easyparking.model.Bahia;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author LEONARDO
 */
@Service
public class BahiaServicioImp implements BahiaServicio {

    @Autowired
    private BahiaDao BahiaDao;
    
    @Override
    @Transactional(readOnly = true) 
    public List<Bahia> listarBahias() {
        return (List<Bahia>) BahiaDao.findAll();
    }
    
}
