package com.mintic.easyparking.servicio;

import com.mintic.easyparking.dao.TipoBahiaDao;
import com.mintic.easyparking.model.TipoBahia;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TipoBahiaServicioImpl implements TipoBahiaServicio {

    @Autowired
    private TipoBahiaDao tipoBahiaDao;

    @Override
    @Transactional(readOnly = true)
    public List<TipoBahia> listarTipoBahias() {
        return (List<TipoBahia>) tipoBahiaDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(TipoBahia tipoBahia) {
        tipoBahiaDao.save(tipoBahia);
    }

    @Override
    @Transactional
    public void eliminar(TipoBahia tipoBahia) {
        tipoBahiaDao.delete(tipoBahia);
    }

    @Override
    @Transactional(readOnly = true)
    public TipoBahia encontrarTipoBahia(TipoBahia tipoBahia) {
        return tipoBahiaDao.findById(tipoBahia.getIdtipos()).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TipoBahia> finAll() {
        return (List<TipoBahia>) tipoBahiaDao.findAll();
    }

    // servicios de RestController
    
    @Override
    @Transactional(readOnly = false)
    public TipoBahia save(TipoBahia tipoBahia) {
        return tipoBahiaDao.save(tipoBahia);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        tipoBahiaDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly=true)
    public TipoBahia findById(Integer id) {
        return tipoBahiaDao.findById(id).orElse(null);
    }

}
