/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.easyparking.servicio;

import com.mintic.easyparking.model.Bahia;
import java.util.List;

/**
 *
 * @author LEONARDO
 */
public interface BahiaServicio {
    
    public List<Bahia> listarBahias();

    
}
