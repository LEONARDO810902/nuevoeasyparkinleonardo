/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.easyparking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mintic.easyparking.model.TipoBahia;

/**
 *
 * @author LEONARDO
 */
public interface TiposBahiaRepository extends JpaRepository <TipoBahia, Integer>{
       
   
}
