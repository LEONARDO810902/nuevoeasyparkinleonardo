/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.easyparking.repository;

import com.mintic.easyparking.model.Bahia;
import com.mintic.easyparking.model.TipoBahia;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author LEONARDO
 */
public interface BahiaRepository extends JpaRepository <Bahia, Integer> {
    
   public TipoBahia findBytipos(String estados);
    
}
