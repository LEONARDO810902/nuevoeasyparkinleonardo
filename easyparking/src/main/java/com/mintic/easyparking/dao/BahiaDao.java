
package com.mintic.easyparking.dao;

import com.mintic.easyparking.model.Bahia;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author LEONARDO
 */


public interface BahiaDao extends CrudRepository <Bahia, Integer>{
    
    public Bahia findByEstado(String estado);
}
