/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mintic.easyparking.dao;

import com.mintic.easyparking.model.TipoBahia;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author LEONARDO
 */
public interface TipoBahiaDao extends CrudRepository <TipoBahia, Integer>{
    
}
