/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mintic.easyparking.RestController;

import com.mintic.easyparking.model.TipoBahia;
import com.mintic.easyparking.servicio.TipoBahiaServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author LEONARDO
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "/resttipobahia")
public class TipoBahiaRestController {

    @Autowired
    private TipoBahiaServicio tipoBahiaServicio;

    @PostMapping(value = "/")
    public ResponseEntity<TipoBahia> save(@RequestBody TipoBahia tipoBahia) {
        TipoBahia obj = tipoBahiaServicio.save(tipoBahia);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/lista")
    public List<TipoBahia> listarTipoBahias() {
        return tipoBahiaServicio.finAll();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<TipoBahia> eliminar(@PathVariable Integer id) {
        TipoBahia obj = tipoBahiaServicio.findById(id);
        if (obj != null) {
            tipoBahiaServicio.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<TipoBahia> editar(@RequestBody TipoBahia tipoBahia) {
        TipoBahia obj = tipoBahiaServicio.findById(tipoBahia.getIdtipos());
        if (obj != null) {
            obj.setIdtipos(tipoBahia.getIdtipos());
            obj.setCodigo(tipoBahia.getCodigo());
            obj.setTipos(tipoBahia.getTipos());
            tipoBahiaServicio.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/lista/{id}")
    public TipoBahia consultaPorId(@PathVariable Integer id) {
        return tipoBahiaServicio.findById(id);
    }

}
