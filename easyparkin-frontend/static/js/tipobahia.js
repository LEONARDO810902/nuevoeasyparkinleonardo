function loadData() {

    let request = sendRequest('resttipobahia/lista', 'GET', '')
    let table = document.getElementById('tipobahia-table');
    table.innerHTML = "";
    request.onload = function () {

        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idtipos}</th>
                    <td>${element.codigo}</td>
                    <td>${element.tipos}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_tipobahia.html?id=${element.idtipos}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteBahia(${element.idtipos})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }

}



function loadtipobahia(idtipos) {
    let request = sendRequest('resttipobahia/lista/' + idtipos, 'GET', '')
    let codigo = document.getElementById('tipobahia-codigo')
    let tipos = document.getElementById('tipobahia-tipos')
    let id = document.getElementById('tipobahia-id')
    request.onload = function () {

        let data = request.response

        id.value = data.idtipos
        codigo.value = data.codigo
        tipos.value = data.tipos
    }


    request.onerror = function () {
        alert("Error al recuperar los datos.");
    }
}




function deleteBahia(idtipos) {
    let request = sendRequest('resttipobahia/' + idtipos, 'DELETE', '')
    request.onload = function () {
        loadData()
    }
}



function saveTipoBahia() {
    let codigo = document.getElementById('tipobahia-codigo').value
    let tipos = document.getElementById('tipobahia-tipos').value
    let id = document.getElementById('tipobahia-id').value
    let data = { 'idtipos': id, 'codigo': codigo, 'tipos': tipos }
    let request = sendRequest('resttipobahia/', id ? 'PUT' : 'POST', data)
    request.onload = function () {
        window.location = 'tipobahia.html';
    }
    request.onerror = function () {
        alert('Error al guardar los cambios.')
    }
}